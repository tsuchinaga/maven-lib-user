import com.tsuchinaga.hellojavalib.HelloWorldFromJava
import com.tsuchinaga.hellokotlinlib.HelloWorldFromKotlin

fun main() {
    println("こんにちわーるど")

    val hwj = HelloWorldFromJava("ライブラリユーザー")
    println(hwj.greeting())

    val hwk = HelloWorldFromKotlin("ライブラリユーザー")
    println(hwk.greeting())
}

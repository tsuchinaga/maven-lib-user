plugins {
    kotlin("jvm") version "1.5.10"
    application
}

group = "com.tsuchinaga.libuser"
version = "0.0.1"

repositories {
    mavenCentral()
    maven {
        url = uri("https://gitlab.com/api/v4/projects/27271935/packages/maven")
        credentials(HttpHeaderCredentials::class) {
            name = "Deploy-Token"
            value = "9pZxYRzy8vqJ9D1xBEsd"
        }
        authentication {
            create<HttpHeaderAuthentication>("header")
        }
    }
    maven("https://gitlab.com/api/v4/projects/27272444/packages/maven")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.tsuchinaga.hellojavalib:hello-java-lib:0.0.2")
    implementation("com.tsuchinaga.hellokotlinlib:hello-kotlin-lib:0.0.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
}

application {
    mainClass.set("MainKt")
}

tasks {
    test {
        useJUnitPlatform()
    }
}
